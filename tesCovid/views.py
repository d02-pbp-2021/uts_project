from datetime import time
import json
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.shortcuts import get_object_or_404, render, redirect
from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse
from tesCovid.models import Covid, Type, City
from .forms import CovidTestForms
from django.template.loader import render_to_string
from account.decorator import allowed_users
from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import TesCovidSerializer


class TesCovidView(APIView):
    def get(self, request):
        covidTests = Covid.objects.all()
        serializer = TesCovidSerializer(covidTests, many = True)
        return Response({"covidTests": serializer.data})
    
    def post(self, request):
        covidTests = request.data.get('covidTests')
        # Create an article from the above data
        serializer = TesCovidSerializer(data=covidTests)
        if serializer.is_valid(raise_exception=True):
            covid_test_saved = serializer.save()

        covidTests = Covid.objects.all()
        serializer = TesCovidSerializer(covidTests, many = True)
        return Response({"covidTests": serializer.data})

    def delete(self, request):
    # Get object with this pk
        covidTitle = request.data.get("title")
        print(covidTitle)
        covidTest = get_object_or_404(Covid.objects.all(), title=covidTitle)
        covidTest.delete()
        return Response({"message": " `{}` has been deleted.".format(covidTest)},status=204)

# Create your views here.
def covid_test(request):
    # https://www.youtube.com/watch?v=-oLVZp1NQVE
    if 'term' in request.GET:
        covidTestsTitle = Covid.objects.filter(title__icontains= request.GET.get('term')) 
        covidTestLoc =  Covid.objects.filter(location__icontains= request.GET.get('term'))
        covvTest = list()
        for test in covidTestsTitle:
            covvTest.append(test.title)
        for test in covidTestLoc:
            covvTest.append(test.location)
        return JsonResponse(covvTest, safe=False)

    covidTests = Covid.objects.all().values()
    types = Type.objects.all().values()
    cities = City.objects.all().values()
    response = {"covidTests": covidTests,
                "types" : types,
                "cities" : cities}
    return render(request, 'tes-covid.html', response)

def add_test(request):
    types = Type.objects.all().values()
    cities = City.objects.all().values()
    response = {"types" : types,
                "cities" : cities}
    context = {}
    form = CovidTestForms(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
        id = Covid.objects.latest('id').id
        title_slug = slugify(Covid.objects.latest('id').title)
        response = redirect('/covid-test/{id}/{title}'.format(id=id, title=title_slug))
        return response

    context['form'] = form
    return render(request, "form.html", context)

# https://www.youtube.com/watch?v=ZHSXaFfx84I&t=453s
def search_test(request):
    query = request.GET['query']
    covidTests = Covid.objects.filter(title__icontains=query) or Covid.objects.filter(location__icontains=query)
    types = Type.objects.all().values()
    cities = City.objects.all().values()
    response = {"covidTests": covidTests,
                "types" : types,
                "cities" : cities}
    return render(request, "search-test-covid.html", response)

def filter_test(request):
    types=request.GET.getlist('type[]')
    cities=request.GET.getlist('city[]')
    allCovidTest=Covid.objects.all().values()
    if len(types)>0:
        allCovidTest=allCovidTest.filter(type__id__in=types)
    if len(cities) > 0:
        allCovidTest=allCovidTest.filter(city__id__in=cities)
    newTestTemplate = render_to_string('filtered-test-list.html',{'covidTests':allCovidTest})
    return JsonResponse({'covidTests': newTestTemplate})

@login_required(login_url='/login')
@allowed_users(allowed_roles=["Customer"])
def test_detail(request, pk, slug):
    covidTest = Covid.objects.get(id=pk)
    response = {
        'covidTest' : covidTest,
    }
    return render(request, 'test-details.html', response)

# https://www.youtube.com/watch?v=8NPOwmtupiI&list=PLCC34OHNcOtr025c1kHSPrnP18YPB-NFi&index=7
def delete_test(request, pk, slug):
    covidTest = Covid.objects.get(id=pk)

    if (request.method =='POST'):
        covidTest.delete()
        return HttpResponseRedirect('/covid-test/')
    response = {"covidTest" : covidTest}
    return render(request, "test-delete.html", response)

