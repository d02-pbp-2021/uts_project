from django.db import models
from django.db.models.fields import TimeField

from django.db.models.fields.files import ImageField

# Create your models here.
class Type(models.Model):
    title=models.CharField(max_length=100)

    def __str__(self):
        return self.title

class City(models.Model):
    title=models.CharField(max_length=100)

    def __str__(self):
        return self.title


    
class Covid(models.Model):
  type = models.ForeignKey(Type, on_delete=models.CASCADE)
  test_image = models.URLField(max_length=300, blank=True, null=True)
  title = models.CharField(max_length=100)
  city = models.ForeignKey(City, on_delete=models.CASCADE)
  price = models.IntegerField()
  location = models.CharField(max_length=100)
  description = models.CharField(max_length=200, default="Tidak ada deskripsi")
  link = models.URLField(max_length=100, blank=True, null=True)
  result_time = models.CharField(max_length=30, blank=True, null=True)
  schedule = models.TimeField(blank=True, null=True)
  phone = models.CharField(max_length=30, blank=True, null=True)
  email = models.EmailField(blank=True, null=True)

  def __str__(self):
      return self.title