from django.test import TestCase, Client
from django.urls import resolve
from .models import Type, City, Covid
from .views import *
from .forms import CovidTestForms
import json
from http import HTTPStatus


# https://riptutorial.com/django/example/4570/testing-django-models-effectively
    
class TypeModelTestCase(TestCase):
    def test_type_created_properly(self):
        type = Type.objects.create(title = "type1")
        self.assertEqual(str(type), type.title)

class CityModelTestCase(TestCase):
    def test_city_created_properly(self):
        city = City.objects.create(title = "city1")
        self.assertEqual(str(city), city.title)

class CovidModelTestCase(TestCase):
    def test_covid_created_properly(self):
        type2 = Type.objects.create(title = "type2")
        city2 = City.objects.create(title = "city2")
        covid = Covid.objects.create(title = "covid_test1", type = type2, city = city2, location = "loc", price = 1)
        self.assertEqual(str(covid), covid.title)


class TestViews(TestCase):
    #Referensi: lab 1 test.py
    def test_covid_test_is_exist(self):
        response = Client().get('/covid-test/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'tes-covid.html')
        self.assertTemplateUsed(response, 'filters.html')

    def test_using_covid_test_func(self):
        found = resolve('/covid-test/')
        self.assertEqual(found.func, covid_test)
    
    def test_form_covid_test_is_exist(self):
        response = Client().get('/covid-test/add')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'form.html')

    def test_using_add_test_func(self):
        found = resolve('/covid-test/add')
        self.assertEqual(found.func, add_test)

    def test_success_filter(self):
        response = self.client.post('/filter-test')
        self.assertEqual(response.status_code, 200)
    
    def test_success_search(self):
        response = self.client.post('/filter-test')
        self.assertEqual(response.status_code, 200)

    def test_form_valid_request(self):
        title2 = "covid test1"
        location2 = "loc"
        price2 = 1
        form_data = {'title':title2,
                    'location':location2,
                    'price':price2,
                    }
        form = CovidTestForms(data=form_data)
        self.assertTrue(form.is_valid)
        
    def test_detail_test_is_exist(self):
        type2 = Type.objects.create(title = "type2")
        city2 = City.objects.create(title = "city2")
        covid = Covid.objects.create(title = "covid test2", type = type2, city = city2, location = "loc", price = 1)
        slug = slugify(covid.title)
        response = Client().get('/covid-test/{id}/{slug}'.format(id=covid.id, slug=slug))
        self.assertEqual(response.status_code, 302)
