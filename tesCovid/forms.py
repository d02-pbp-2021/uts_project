from django import forms
from .models import Covid


class CovidTestForms(forms.ModelForm):
    class Meta:
        model = Covid
        
        fields = ('type', 'title', 'city', 'price', 'location', 'description', 'link', 'result_time', 'schedule', 'phone', 'email', 'test_image',)

        widgets = {
            'type': forms.Select(attrs={'class': 'entry form-select'}),
            'title': forms.TextInput(attrs={'class': 'entry formEntry'}),
            'city': forms.Select(attrs={'class': 'entry form-select'}),
            'price': forms.NumberInput(attrs={'class': 'entry formEntry'}),
            'location': forms.TextInput(attrs={'class': 'entry formEntry'}),
            'description': forms.Textarea(attrs={'class': 'entry formEntry'}),
            'link': forms.URLInput(attrs={'class': 'entry formEntry'}),
            'title': forms.TextInput(attrs={'class': 'entry formEntry'}),
            'result_time': forms.TextInput(attrs={'class': 'entry formEntry'}),
            'schedule': forms.TimeInput(attrs={'class': 'entry formEntry'}),
            'phone': forms.TextInput(attrs={'class': 'entry formEntry'}),
            'email': forms.EmailInput(attrs={'class': 'entry formEntry'}),
        }