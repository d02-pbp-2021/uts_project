from django.apps import AppConfig


class TescovidConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tesCovid'
