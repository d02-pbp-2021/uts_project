from django.contrib import admin
from .models import Covid, Type, City

# Register your models here.
admin.site.register(Covid)
admin.site.register(Type)
admin.site.register(City)

