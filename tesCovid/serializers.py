from rest_framework import serializers

from tesCovid.models import City, Covid, Type


class TesCovidSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100)
    type = serializers.CharField(max_length=100)
    test_image = serializers.URLField(max_length=300, allow_null = True, allow_blank = True)
    city = serializers.CharField(max_length=100)
    price = serializers.IntegerField()
    location = serializers.CharField(max_length=100)
    description = serializers.CharField(max_length=200, allow_null = True, allow_blank = True, default="Tidak ada deskripsi")
    link = serializers.URLField(max_length=100, allow_null = True)
    result_time = serializers.CharField(max_length=30, allow_null = True, allow_blank = True )
    schedule = serializers.TimeField(allow_null = True)
    phone = serializers.CharField(max_length=30, allow_null = True, allow_blank = True)
    email = serializers.EmailField(allow_null = True, allow_blank = True)

    def create(self, validated_data):
        print(validated_data)
        validated_data['type'] = Type.objects.get(title = validated_data['type'])
        validated_data['city'] = City.objects.get(title = validated_data['city'])
        return Covid.objects.create(**validated_data)