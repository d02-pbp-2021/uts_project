# Generated by Django 3.2.8 on 2021-10-26 14:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tesCovid', '0004_auto_20211026_2133'),
    ]

    operations = [
        migrations.AlterField(
            model_name='covid',
            name='result_time',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
    ]
