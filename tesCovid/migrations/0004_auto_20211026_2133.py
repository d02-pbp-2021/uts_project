# Generated by Django 3.2.8 on 2021-10-26 14:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tesCovid', '0003_auto_20211026_1915'),
    ]

    operations = [
        migrations.AddField(
            model_name='covid',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
        migrations.AddField(
            model_name='covid',
            name='phone',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='covid',
            name='result_time',
            field=models.DurationField(blank=True, max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='covid',
            name='schedule',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
