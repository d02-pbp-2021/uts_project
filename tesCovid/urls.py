from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from .views import TesCovidView

from .views import covid_test, add_test, test_detail, delete_test, search_test

urlpatterns = [
    path('', covid_test, name='covid_test'),
    path('add', add_test, name='add_test'),
    path('search/', search_test, name="search-test"),
    path('<int:pk>/<str:slug>',test_detail, name='detail-tes-covid'),
    path('<int:pk>/<str:slug>/delete', delete_test, name='delete-test'),
    path('kirim-data', TesCovidView.as_view()),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)