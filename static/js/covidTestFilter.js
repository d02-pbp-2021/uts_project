// https://www.youtube.com/watch?v=Op_A8JdieVQ
$(document).ready(function(){
    $(".ajaxLoader").hide();
    $(".filter-checkbox").on('click', function(){
        var _filterArr= {};
        $(".filter-checkbox").each(function(index, ele){
            var _filterVal = $(this).val();
            var _filterKey = $(this).data('filter');
            _filterArr[_filterKey] = Array.from(document.querySelectorAll('input[data-filter = '+_filterKey+']:checked')).map(function(element){
                return element.value;
            });
        });
        console.log(_filterArr);
        //ajax
        $.ajax({
            url:'/filter-test',
            data:_filterArr,
            dataType:'json',
            beforeSend:function(){
                $(".ajaxLoader").show();
            },
            success:function(res){
                console.log(res);
                $(".cards").html(res.covidTests);
                $(".ajaxLoader").hide();
            }
        });
    })

});