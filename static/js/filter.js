$(document).ready(function() {
    $(".ajaxLoader").hide();
	$(".filter").on('click',function(){
		var filterTempat={};
		$(".filter").each(function(index,ele){
			var filterKey=$(this).data('filter');
			filterTempat[filterKey]=Array.from(document.querySelectorAll('input[data-filter='+filterKey+']:checked')).map(function(el){
			 	return el.value;
			});
		});

        // Untuk lihat isi array filter
        // console.log(filterTempat);

        $.ajax({
			url:'filter-data',
			data:filterTempat,
			dataType:'json',
			beforeSend:function(){
				$(".ajaxLoader").show();
			},
			success:function(res){
				console.log(res);
				$("#filteredTempat").html(res.data);
				$(".ajaxLoader").hide();
			}
		});
	});

});