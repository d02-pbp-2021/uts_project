from django.shortcuts import render
from django.template.loader import render_to_string
from django.http import JsonResponse, HttpResponseRedirect, request, HttpResponse
from django.contrib.auth.decorators import login_required
from .models import TempatVaksin, Kota, JenisVaksin
from .forms import TempatForm
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import TempatVaksinSerializer

# Create your views here.
def list_tempat(request):
    tempat = TempatVaksin.objects.all().order_by('-id').distinct()
    banyakTempat = TempatVaksin.objects.count()
    kota = Kota.objects.all().values()
    jenis = JenisVaksin.objects.all().values()
    response = {'data' : tempat, 'kota' : kota, 'jenis' : jenis, 'banyakData' : banyakTempat}
    return render(request, 'list_tempat.html', response)

def filter_data(request):
    kota = request.GET.getlist('kota[]')
    jenis = request.GET.getlist('jenis[]')
    semuaTempat = TempatVaksin.objects.all().order_by('-id').distinct()
    if len(kota) > 0:
        semuaTempat = semuaTempat.filter(Kota__id__in = kota).distinct()
    if len(jenis) > 0:
        semuaTempat = semuaTempat.filter(JenisVaksin__id__in = jenis).distinct()
    tempat = render_to_string('list-tempat.html', {'data' : semuaTempat})
    return JsonResponse({'data' : tempat})

def search(request):
    banyakTempat = TempatVaksin.objects.count()
    tempatDicari = request.GET['cari']
    kota = Kota.objects.all().values()
    jenis = JenisVaksin.objects.all().values()
    tempat = TempatVaksin.objects.all().order_by('-id').distinct()
    tempatFiltered = TempatVaksin.objects.filter(Nama__icontains = tempatDicari).order_by('-id')
    if tempatDicari == '':
        response = {'data': tempat, 'kota' : kota, 'jenis' : jenis, 'banyakData' : banyakTempat}
        return render(request, 'list_tempat.html', response)
    else:
        response = {'data' : tempatFiltered, 'kota' : kota, 'jenis' : jenis, 'banyakData' : banyakTempat}
        return render(request, 'search_tempat.html', response)

def tambah_tempat(request):
    form = TempatForm(request.POST, request.FILES)
    response = {'form' : form}
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/daftar-vaksin')
    else:
        return render(request, 'form_tempat.html', response)

# Web Service UAS
@csrf_exempt
def json(request):
    products = TempatVaksin.objects.all()
    data = serializers.serialize('json', products)
    return HttpResponse(data, content_type="application/json")

class DaftarVaksinView(APIView):
    def get(self, request):
        lokasiVaksin = TempatVaksin.objects.all()
        serializer = TempatVaksinSerializer(lokasiVaksin, many = True)
        return Response({"lokasiVaksin": serializer.data})
    
    def post(self, request):
        lokasiVaksin = request.data.get('lokasiVaksin')

        serializer = TempatVaksinSerializer(data = lokasiVaksin)
        if serializer.is_valid(raise_exception=True):
            lokasiVaksin_saved = serializer.save()

        lokasiVaksin = TempatVaksin.objects.all()
        serializer = TempatVaksinSerializer(lokasiVaksin, many = True)
        return Response({"lokasiVaksin": serializer.data})