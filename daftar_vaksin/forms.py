from .models import TempatVaksin
from django import forms

class TempatForm(forms.ModelForm):
    class Meta:
        model = TempatVaksin
        fields = ('Nama', 'Lokasi', 'Kota', 'JenisVaksin', 'Jadwal', 'LinkDaftar', 'Foto')

        widgets = {
            'Nama': forms.TextInput(attrs={'class': 'entry formEntry'}),
            'Lokasi': forms.TextInput(attrs={'class': 'entry formEntry'}),
            'Kota': forms.Select(attrs={'class': 'entry form-control'}),
            'JenisVaksin': forms.Select(attrs={'class': 'entry form-control'}),
            'Jadwal': forms.Textarea(attrs={'class': 'entry formEntry'}),
            'LinkDaftar': forms.URLInput(attrs={'class': 'entry formEntry'}),
            'Foto': forms.URLInput(attrs={'class': 'entry formEntry'}),
        }