from os import stat
from django.urls import path
from .views import list_tempat, filter_data, search, tambah_tempat, json
from .views import DaftarVaksinView

from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', list_tempat, name='list_tempat'),
    path('filter-data', filter_data, name='filter_data'),
    path('search', search, name='search'),
    path('tambah-tempat', tambah_tempat, name='tambah_tempat'),
    path('json/', json, name='json'),
    path('data', DaftarVaksinView.as_view()),
    path('articles/<int:pk>', DaftarVaksinView.as_view()),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)