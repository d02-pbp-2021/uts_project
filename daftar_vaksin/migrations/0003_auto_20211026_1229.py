# Generated by Django 3.2.8 on 2021-10-26 12:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('daftar_vaksin', '0002_tempatvaksin_linkdaftar'),
    ]

    operations = [
        migrations.AddField(
            model_name='tempatvaksin',
            name='Jadwal',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='tempatvaksin',
            name='LinkDaftar',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
