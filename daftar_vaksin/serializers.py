from rest_framework import serializers
from .models import TempatVaksin, Kota, JenisVaksin

class TempatVaksinSerializer(serializers.Serializer):
    Nama = serializers.CharField(max_length = 50)
    Lokasi = serializers.CharField(max_length = 50)
    Kota = serializers.CharField(max_length = 50)
    JenisVaksin = serializers.CharField(max_length = 50)
    Jadwal = serializers.CharField(style={'base_template': 'textarea.html'})
    LinkDaftar = serializers.URLField(max_length = 100, allow_null = True)
    Foto = serializers.URLField(max_length = 100, allow_null = True)

    def create(self, validated_data):
        validated_data['JenisVaksin'] = JenisVaksin.objects.get(Nama = validated_data['JenisVaksin'])
        validated_data['Kota'] = Kota.objects.get(Nama = validated_data['Kota'])
        return TempatVaksin.objects.create(**validated_data)