from django.contrib import admin

# Register your models here.
from .models import TempatVaksin, Kota, JenisVaksin
admin.site.register(TempatVaksin)
admin.site.register(Kota)
admin.site.register(JenisVaksin)