from django.db import models

class JenisVaksin(models.Model):
    Nama = models.CharField(max_length = 50)

    def __str__(self):
        return self.Nama

class Kota(models.Model):
    Nama = models.CharField(max_length = 50)

    def __str__(self):
        return self.Nama

class TempatVaksin(models.Model):
    Nama = models.CharField(max_length = 50, default = "Nama")
    Lokasi = models.CharField(max_length = 50, default="Lokasi")
    Kota = models.ForeignKey(Kota, on_delete = models.CASCADE, blank=True, null=True)
    JenisVaksin = models.ForeignKey(JenisVaksin, on_delete = models.CASCADE, blank=True, null=True)
    Jadwal = models.TextField(blank=True, null=True)
    LinkDaftar = models.URLField(blank=True, null=True)
    Foto = models.URLField(blank=True, null=True)
    
    def __str__(self):
        return self.Nama