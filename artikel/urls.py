import json
from django.urls import path
from .views import delete_artikel, delete_komentar, index, detail, add_artikel, update_artikel, delete_artikel, kategori, \
    like_artikel, komentar_artikel, load_more, search_artikel, autocomplete, delete_komentar, load_more_search, load_more_kategori, \
    fetch_data, fetch_data_terpopuler, fetch_data_kategori, post_data, post_delete_data

urlpatterns = [
    path('', index, name='index-artikel'),
    path('<int:pk>/<str:slug>', detail, name='detail-artikel'),
    path('form/', add_artikel, name='add-artikel'),
    path('edit/<int:pk>', update_artikel, name='update-artikel'),
    path('delete/<int:pk>', delete_artikel, name='delete-artikel'),
    path('category/<str:kategori>', kategori, name='kategori-artikel'),
    path('like/<int:pk>', like_artikel, name='like-artikel'),
    path('komentar/<int:pk>', komentar_artikel, name='komentar-artikel'),
    path('load-more/', load_more, name='load-more'),
    path('search/', search_artikel, name='search-artikel'),
    path('autocomplete/', autocomplete, name='autocomplete'),
    path('delete-komentar/<int:pk>', delete_komentar, name='delete-komentar'),
    path('load-more-search/', load_more_search, name='load-more-search'),
    path('load-more-kategori/', load_more_kategori, name='load-more-kategori'),
    path('fetch_data/', fetch_data, name='artikel-fetch-data'),
    path('fetch_data_terpopuler/', fetch_data_terpopuler, name='artikel-fetch-data-terpopuler'),
    path('fetch_data_kategori/', fetch_data_kategori, name='artikel-fetch-data-kategori'),
    path('post_data/', post_data, name='post-data'),
    path('post_delete_data/', post_delete_data, name='post-delete-data'),
    
]

