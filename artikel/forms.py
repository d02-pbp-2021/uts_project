from django import forms
from django.db.models import fields
from django.http import request
from .models import Artikel, Kategori, Komentar

choices = list()

try:
    request = Kategori.objects.values_list('nama', 'nama')
    for item in request:
        choices.append(item)

except:
    choices = list()

class ArtikelForm(forms.ModelForm):
    class Meta:
        model = Artikel
        fields = [
            'judul',
            'isi',
            'ringkasan',
            'gambar',
            'kategori',
        ]

        widgets = {
            'kategori': forms.Select(choices=choices, attrs={
                'class': "form-select"
            }),
            'isi': forms.Textarea(attrs=
            {
                'class': 'form-control',
                'rows': 15,
            }),
            'ringkasan': forms.Textarea(attrs=
            {
                'class': "form-control",
                'maxlength': 240,
                'rows': 3,
            }),
            'gambar': forms.URLInput(attrs=
            {
                'class': "form-control"
            })
        }   

class KomentarForm(forms.ModelForm):
    class Meta:
        model = Komentar
        fields = [
            'isi'
        ]
