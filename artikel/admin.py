from django.contrib import admin

# Register your models here.
from .models import Artikel, Kategori, Komentar

admin.site.register(Artikel)
admin.site.register(Kategori)
admin.site.register(Komentar)
