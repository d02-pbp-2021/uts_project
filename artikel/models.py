from django.db import models
from django.contrib.auth.models import User
from autoslug import AutoSlugField
from datetime import datetime, date
from ckeditor.fields import RichTextField
from django.template.defaultfilters import slugify

# Create your models here.
class Artikel(models.Model):
    judul = models.CharField(max_length=255)
    # isi = models.TextField()
    isi = models.TextField()
    ringkasan = models.TextField()
    tanggal = models.DateField(auto_now_add=True)
    gambar = models.URLField()
    slug = AutoSlugField(populate_from='judul')
    kategori = models.CharField(max_length=255)
    likes = models.ManyToManyField(User, related_name='artikel_like')
    jumlah_like = models.PositiveIntegerField(default=0)
    nama_penulis = models.CharField(max_length=255)
    # https://pypi.org/project/django-autoslug/

    def __str__(self):
        return self.judul + " | " + str(self.penulis)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.judul)
        return super(Artikel, self).save(*args, **kwargs)

    def total_likes(self):
        return self.likes.count()

    def update_likes(self):
        self.jumlah_like = self.total_likes()
        self.save()

class Kategori(models.Model):
    nama = models.CharField(max_length=255)

    def __str__(self):
        return self.nama

class Komentar(models.Model):
    artikel = models.ForeignKey(Artikel, related_name='komen', on_delete=models.CASCADE)
    penulis = models.ForeignKey(User, on_delete=models.CASCADE)
    isi = models.TextField()
    tanggal = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{judul} - {penulis}'.format(judul=self.artikel.judul, penulis=self.nama_penulis)
