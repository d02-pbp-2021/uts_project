from django.db.models.expressions import F
from django.http import response
from django.http.request import QueryDict
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db.models import Count, Q
from django.template.loader import render_to_string
# from django.views.generic import ListView, DetailView
from django.contrib.auth.decorators import login_required
from django.views.decorators import csrf
from .models import Artikel, Komentar, Kategori
from .forms import ArtikelForm, KomentarForm
from django.middleware.csrf import get_token
from account.decorator import allowed_users
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
import json

# https://www.youtube.com/watch?v=B40bteAMM_M&list=PLCC34OHNcOtr025c1kHSPrnP18YPB-NFi&index=2&ab_channel=Codemy.com

def index(request):
    # artikels = Artikel.objects.annotate(like_count=Count('likes')).order_by('-like_count')[:4]
    artikels = Artikel.objects.order_by('-tanggal')[:4]
    total_artikel = Artikel.objects.count()

    response = {
        'artikels': artikels,
        'total_artikel': total_artikel,
        'load_button': True,
    }

    if len(artikels) >=3:
        response['top_1'] = artikels[0]
        response['top_2'] = artikels[1]
        response['top_3'] = artikels[2]
    
    # if len(artikels) > 4:
    #     response['load_button'] = True

    return render(request, 'index.html', response)

def detail(request, pk, slug):
    artikel = Artikel.objects.get(id=pk)
    liked = False

    if artikel.likes.filter(id=request.user.id).exists():
        liked = True
    
    response = {
        'artikel': artikel,
        'likes': artikel.total_likes(),
        'liked': liked
    }

    return render(request, 'details.html', response)

@login_required(login_url='/login')
@allowed_users(allowed_roles=["Author"])
def add_artikel(request):
    artikel = Artikel(nama_penulis=request.user.get_full_name())
    form = ArtikelForm(request.POST or None, request.FILES, instance=artikel)
    if form.is_valid() and request.method == 'POST':
        form.save()
        # http://www.learningaboutelectronics.com/Articles/How-to-redirect-a-user-from-create-view-to-detail-view-in-Django.php
        return HttpResponseRedirect('/artikel/{id}/{slug}'.format(id = artikel.id, slug=artikel.slug))

    response = {
        'artikel': artikel,
        'form': form,
    }

    return render(request, 'form-artikel.html', response)

# https://www.youtube.com/watch?v=EX6Tt-ZW0so&ab_channel=DennisIvy
@login_required(login_url='/login')
@allowed_users(allowed_roles=["Author"])
def update_artikel(request, pk):
    artikel = Artikel.objects.get(id=pk)
    form = ArtikelForm(request.POST or None, instance=artikel)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return HttpResponseRedirect('/artikel/{id}/{slug}'.format(id=artikel.id,slug=artikel.slug))

    response = {
        'artikel': artikel,
        'form': form,
    }

    return render(request, 'form-artikel.html', response)

@login_required(login_url='/login')
@allowed_users(allowed_roles=["Author"])
def delete_artikel(request, pk):
    artikel = Artikel.objects.get(id=pk)
    if request.method == "POST":
        artikel.delete()
        return HttpResponseRedirect('/artikel')

    response = {
        'artikel': artikel
    }

    return render(request, 'delete.html', response)

@login_required(login_url='/login')
def like_artikel(request, pk):
    # artikel = get_object_or_404(Artikel, id=request.POST.get('artikel-id'))
    artikel = Artikel.objects.get(id=pk)

    if artikel.likes.filter(id=request.user.id).exists():
        artikel.likes.remove(request.user),
    else:
        artikel.likes.add(request.user)

    Artikel.objects.get(id=pk).update_likes()

    return HttpResponseRedirect('/artikel/{id}/{slug}'.format(id=artikel.id,slug=artikel.slug))

@login_required(login_url='/login')
def komentar_artikel(request, pk):
    komentar = Komentar(penulis=request.user)
    komentar.artikel = Artikel.objects.get(id=pk)

    liked = False

    if komentar.artikel.likes.filter(id=request.user.id).exists():
        liked = True

    form = KomentarForm(request.POST or None, instance=komentar)
    if form.is_valid() and request.method == 'POST':
        form.save()
        return HttpResponseRedirect('/artikel/{id}/{slug}'.format(id=komentar.artikel.id,slug=komentar.artikel.slug))

    response = {
        'komentar': komentar,
        'artikel': komentar.artikel,
        'form': form,
        'likes': komentar.artikel.total_likes(),
        'liked': liked
    }

    return render(request, 'komen.html', response)

@login_required(login_url='/login')
def delete_komentar(request, pk):
    komentar = Komentar.objects.get(id=pk)
    if request.method == "POST":
        komentar.delete()
        return HttpResponseRedirect('/artikel/{id}/{slug}'.format(id=komentar.artikel.id,slug=komentar.artikel.slug))

def load_more(request):
    if request.is_ajax():
        offset = int(request.GET['offset'])
        limit = int(request.GET['limit'])
        csrf_token = request.GET['csrfmiddlewaretoken']
        # artikels = Artikel.objects.annotate(like_count=Count('likes')).order_by('-like_count')[offset:offset+limit]
        artikels = Artikel.objects.order_by('-tanggal')[offset:offset+limit]
        user = request.user
        data = render_to_string('ajax/list-artikel.html',{'artikels':artikels, 'user':user, 'csrf_token':csrf_token})
        return JsonResponse({'data':data})

def load_more_search(request):
    if request.is_ajax():
        offset = int(request.GET['offset'])
        limit = int(request.GET['limit'])
        csrf_token = request.GET['csrfmiddlewaretoken']
        search = str(request.GET['term'])
        # artikels = Artikel.objects.annotate(like_count=Count('likes')).order_by('-like_count')[offset:offset+limit]
        artikels = Artikel.objects.filter(Q(judul__icontains=search)|Q(ringkasan__icontains=search)|Q(isi__icontains=search)|Q(kategori__icontains=search))[offset:offset+limit]
        user = request.user
        data = render_to_string('ajax/list-artikel.html',{'artikels':artikels, 'user':user, 'csrf_token':csrf_token})
        return JsonResponse({'data':data})

def load_more_kategori(request):
    if request.is_ajax():
        offset = int(request.GET['offset'])
        limit = int(request.GET['limit'])
        csrf_token = get_token(request)
        search = str(request.GET['term'])
        # artikels = Artikel.objects.annotate(like_count=Count('likes')).order_by('-like_count')[offset:offset+limit]
        artikels = Artikel.objects.filter(kategori__icontains=search)[offset:offset+limit]
        user = request.user
        data = render_to_string('ajax/list-artikel.html',{'artikels':artikels, 'user':user, 'csrf_token':csrf_token})
        return JsonResponse({'data':data})


def kategori(request, kategori):
    artikels = Artikel.objects.filter(kategori=kategori.replace('-', ' '))[:4]
    total_artikel = Artikel.objects.count()
    response = {
        'artikels': artikels,
        'kategori': kategori.title().replace('-', ' '),
        'total_artikel': total_artikel,
        'load_button': True,
    }

    # if len(artikels) > 4:
    #     response['load_button'] = True

    return render(request, 'kategori.html', response)

def search_artikel(request):
    if request.method == 'POST':
        search = request.POST.get('searched', False)
        # artikels = Artikel.objects.filter(judul__contains=search).filter(ringkasan__contains=search).filter(isi__contains=search)
        artikels = Artikel.objects.filter(Q(judul__icontains=search)|Q(ringkasan__icontains=search)|Q(isi__icontains=search)|Q(kategori__icontains=search))[:4]
        total_artikel = Artikel.objects.filter(Q(judul__icontains=search)|Q(ringkasan__icontains=search)|Q(isi__icontains=search)|Q(kategori__icontains=search)).count()
        response = {
            'search': search,
            'artikels': artikels,
            'total_artikel': total_artikel,
            'load_button': True,
        }

        # if len(artikels) > 4:
        #     response['load_button'] = True

        return render(request, 'search.html', response)
    else:
        return render(request, 'search.html', {})

def autocomplete(request):
    if 'term' in request.GET:
        term = request.GET.get('term', False)
        artikels = Artikel.objects.filter(Q(judul__istartswith=term))
        kategori = Kategori.objects.filter(Q(nama__istartswith=term))
        list_artikel = list()
        for artikel in artikels:
            if artikel.judul not in list_artikel:
                list_artikel.append(artikel.judul)

        for kat in kategori:
            if kat.nama not in list_artikel:
                list_artikel.append(kat.nama)

        return JsonResponse(list_artikel, safe=False)

    return render(request, 'index.html')


@csrf_exempt
def fetch_data(request):
    artikels = Artikel.objects.all().order_by('-tanggal')
    data = serializers.serialize('json', artikels)
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def fetch_data_terpopuler(request):
    artikels = Artikel.objects.all().order_by('-jumlah_like')
    data = serializers.serialize('json', artikels)
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def fetch_data_kategori(request):
    kategoris = Kategori.objects.all()
    data = serializers.serialize('json', kategoris)
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def post_data(request):
    if request.method == "POST":
        req_data = json.loads(request.body.decode('utf-8'))
        data = QueryDict('', mutable=True)
        data.update(req_data)

        try:
            nama_penulis = request.user.get_full_name()
        except:
            nama_penulis = "Anonymous"

        artikel = Artikel(nama_penulis=nama_penulis)
        form = ArtikelForm(data, instance=artikel)

        if form.is_valid():
            form.save()

        return HttpResponseRedirect('/artikel')

@csrf_exempt
def post_delete_data(request):
    if request.method == "POST":
        pk = json.loads(request.body.decode('utf-8'))['pk']
        print(pk)
        artikel = Artikel.objects.get(id=int(pk))
        artikel.delete()

        return HttpResponseRedirect('/artikel')



    