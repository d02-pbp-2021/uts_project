from django.db import models
from django.contrib.auth.models import User
# Create your models here.

rolechoices = (
  ('Customer','Customer'),
  ('Seller', 'Seller'),
  ('Author', 'Author')
)

class Profile(models.Model):
  user =  models.OneToOneField(User, on_delete=models.CASCADE, null = True)
  role = models.CharField(max_length=10, choices=rolechoices, default='Customer')
