from django.shortcuts import render,redirect
from .forms import ProfileForm, CreateUserForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import Group, User
from account.decorator import unauthenticated_user, allowed_users
from django.contrib.auth.decorators import login_required
from django.views import View
import json
from django.http import JsonResponse, HttpResponse
from .models import Profile
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
@unauthenticated_user
def registerPage(request):
  prof_form = ProfileForm()
  form = CreateUserForm()
  if request.is_ajax() :
    prof_form = ProfileForm(request.POST)
    form = CreateUserForm(request.POST)
    if form.is_valid() and prof_form.is_valid():
      
      user = form.save()
      user.save()
      profile =prof_form.save()
      profile.user = user
      profile.save()

      role = request.POST.get('role')
      group = Group.objects.get(name=role)
      user.groups.add(group)

      return JsonResponse({
        'msg':'Success'
      })
  context = {'form':form, 'prof_form':prof_form}
  return render(request, 'register2.html', context)

@unauthenticated_user
def loginPage(request):
  if request.method == 'POST':
    username = request.POST.get('username')
    password = request.POST.get('password')

    user = authenticate(request, username=username, password=password)

    if user is not None:
      login(request, user)
      return redirect('/')
    else:
      messages.info(request, 'Username OR password is incorrect')
  context = {}
  return render(request, 'login.html', context)
@login_required(login_url='/login')
def logoutUser(request):
  logout(request)
  return redirect('login')

@csrf_exempt
def loginFlutter(request):
  data = json.loads(request.body)
  username = data['username']
  password = data['password']
  print(username)
  print(password)

  user= authenticate(username=username, password=password)
  print(user)
  
  if user is not None:
    if user.is_superuser:
      role = 'Admin'
    else:
      role = user.groups.all()[0].name
    return JsonResponse({'status':'success', 'role':role})
  else:
    return JsonResponse({'status':'failed'})


@csrf_exempt
def regFlutter(request):
  data = json.loads(request.body)
  dataUser={
    'username':  data['username'],
    'password1': data['password1'],
    'password2': data['password2']
  }
  print(dataUser)
  dataProf={
    'role':data['role']
  }
  print(dataProf)
  form = CreateUserForm(dataUser or None)
  prof = ProfileForm(dataProf or None)

  if data['password1']==data['password2']:
    user = form.save()
  
    profile = prof.save()
    profile.user = user
    profile.save()
    role = data['role']
    group = Group.objects.get(name=role)
    user.groups.add(group)

    return JsonResponse({
      'status': 'success'
    })
  else:
    return JsonResponse({
      'status': 'failed'
    })