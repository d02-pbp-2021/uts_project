from django.urls import path
from .views import *
urlpatterns = [
    path('register/',registerPage,name='register'),
    path('login/',loginPage,name='login'),
    path('logout/',logoutUser,name='logout'),
    path('login_apk/',loginFlutter, name='login_apk'),
    path('reg_apk/',regFlutter,name='reg_apk'),
]