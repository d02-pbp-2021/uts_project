from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import Profile


class CreateUserForm(UserCreationForm):
  class Meta:
    model= User
    fields = ['username','password1','password2']
    widgets = {
      'username':forms.TextInput(attrs={
        'id':'post-text'
      })
    }

class ProfileForm(forms.ModelForm):
  class Meta:
    model = Profile
    fields =['role']
    widgets={
      'role':forms.RadioSelect(attrs={'class':'custom-control-input'})
    }