# PROJECT UTS D02 PBP 2021

## Daftar isi

- [Daftar Isi](#daftar-isi)
- [Anggota Kelompok](#anggota-kelompok)
- [Link Herokuapp](#link-herokuapp)
- [Aplikasi yang Diajukan](#aplikasi-yang-diajukan)
- [Persona Pengguna](#persona-pengguna)

## Anggota Kelompok

1. Arya Bintang Pratama Kumaladjati (2006462960)
2. Judah Ariesaka Magaini (2006463042)
3. Kenneth Jonathan (2006463364)
4. Farah Dita Ashilah (2006463723)
5. Melati Eka Putri (2006464266)
6. David Johan Hasiholan Parhusip (2006482211)
7. Adristi Marsalma Nectarine Amaranthi (2006483113)

## Link Herokuapp
**Link Heroapp kelompok**: https://lindungipeduli.herokuapp.com/

## Aplikasi yang Diajukan
1. **Tema**		: Aplikasi E-Health
2. **Judul**	: Lindungi Peduli
3. **Manfaat**	:
   Sebagai wadah yang menyediakan informasi seputar Covid-19, termasuk informasi terkini, pendaftaran vaksin, apotek daring sebagai penyedia obat-obatan, hingga informasi kesehatan lain yang dikemas dalam bentuk artikel. 
4. **Daftar Modul** :
   - Pengaturan Pengguna
      - **Developer** : Arya Bintang Pratama K. 
      - **Deskripsi** : Fitur Login dan Register akun, dengan 3 role yaitu Author, Customer, Seller. terdapat authentication untuk membedakan role apa saja yang bisa masuk ke page tertentu
      - **Detail Fitur** : Arya Bintang Pratama K. 
        - Login Page
        - Register Page
        - Authentication User
   - Home Page
      - **Developer** : Kenneth Jonathan
      - **Deskripsi** : Home Page menampilkan informasi seputar COVID-19, serta topik-topik diskusi yang dihubungkan dengan forum untuk berdiskusi terkait topik tersebut.
      - **Detail Fitur** :
         - Landing Page (Kondisi COVID-19 dan Topik-topik diskusi)
         - Halaman forum diskusi
         - Halaman untuk menambah topik forum
   3. Fitur Artikel
      - **Developer**: Judah Ariesaka Magaini
      - **Deskripsi**: Fitur ini menyediakan artikel-artikel tentang kesehatan. Setiap pengunjung dapat membaca dan mencari artikel, namun hanya pengguna yang telah login yang dapat menyukai artikel dan memberi komentar. Untuk menulis dan menghapus artikel, pengguna harus login sebagai 'Author' terlebih dahulu. Hanya admin yang dapat menambahkan kategori artikel.
      - **Detail fitur**:
         - Tampilan utama artikel
         - Halaman detail artikel
         - Fitur comment dan like pada setiap artikel
         - Pencarian artikel berdasarkan kata kunci dan kategori
   4. Kamus Obat
      - **Developer**: Farah Dita Ashilah
      - **Deskripsi**: Fitur ini menyediakan tampilan mengenai obat-obatan yang disertai dengan deskripsi, efek samping, dan komposisi obat. Fitur ini dilengkapi dengan search bar dan load more pagination. Untuk menambahkan deskripsi obat 
      atau menghapus kamus obat, pengguna perlu log in terlebih dahulu sebagai admin.
      - **Detail fitur**:
            - Tampilan utama daftar kamus obat
            - Halaman detail kamus obat
            - Form untuk menambahkan kamus obat dan menghapus kamus obat
   5. Apotek Online
      - **Developer**: Adristi Marsalma Nectarine Amaranthi
      - **Deskripsi**: Fitur ini menyediakan tampilan produk apotek dengan deskirpsi singkat dan link pembelian obat. Fitur ini dilengkapi dengan search bar. Hanya pengguna dengan role Seller dan Admin saja yang dapat menambahkan obat.
      - **Detail fitur**:
         - Tampilan halaman katalog apotek
         - Form untuk menambahkan produk
   6. Daftar Tes Covid : 
      - **Developer**: Melati Eka Putri
      - **Deskripsi**: Fitur ini menyedikan pilihan tes covid yang dapat difilter sesuai tipe dan lokasi tes covid yang dipilih. Pengguna juga bisa menggunakan fitur pencarian untuk mencari tes covid berdasarkan judul atau lokasi. Untuk melihat detail tes covid, pengguna harus login sebagai 'Customer' terlebih dahulu. Hanya admin yang dapat menambahkan dan menghapus tes covid.
      - **Detail fitur**:
         - Tampilan utama daftar tes covid
         - Filter tes covid berdasarkan tipe dan lokasi
         - Pencarian tes covid berdasarkan judul atau lokasi
         - Halaman detail tes covid
         - Form untuk menambahkan daftar tes covid
   7. Pendaftaran Vaksin 
      - **Developer**: David Johan Hasiholan Parhusip
      - **Deskripsi**: Fitur ini menyedikan pilihan lokasi vaksin yang dapat difilter sesuai tipe vaksin dan kota yang diinginkan. Lokasi pendaftaran vaksin juga dapat dicari berdasarkan nama dari lokasi yang diinginkan. Hanya admin yang dapat menambahkan dan menghapus lokasi vaksin.
      - **Detail fitur**:
         - Tampilan utama daftar lokasi vaksin
         - Form untuk menambahkan lokasi vaksin
         - Filter lokasi berdasarkan tipe vaksin dan kota
         - Kotak pencarian lokasi berdasarkan nama

## Persona Pengguna
Berikut merupakan persona dari pengguna situs web Lindungi Peduli:
   - Visitor **(V)** : Pengguna yang belum login, memiliki akses yang paling dibatasi
   - Login User **(L)** : Pengguna yang sudah login dan bisa mengakses semua fitur. Namun tidak punya otoritas khusus
   - Article Author **(Aut)**: Pengguna khusus yang berperan sebagai penulis artikel pada situs web
   - Seller **(S)**: Pengguna khusus yang berperan sebagai penyedia obat-obatan yang dijual pada situs web
   - Admin **(Adm)**: Pengguna yang memiliki akses ke semua fitur (baik otoritas khusus atau bukan) dan memiliki wewenang untuk menghapus suatu konten/produk

**Akses setiap persona**:
1. Halaman Login: **Semua bisa mengakses**
2. Halaman Register: **V, Adm**
3. Pengaturan pengguna: **L, Aut, S, Adm**
4. Fitur penambahan obat: **S, Adm**
5. Mengontrol obat yang dijual: **Adm**
6. Melihat kamus dan katalog obat: **V, L, S, Adm**
7. Melihat halaman utama tes covid: **V, L, Adm**
8. Melihat halaman detail tes covid: **L, Adm, Aut, S**
8. Menulis artikel: **Aut, Adm**
9. Mengontrol artikel terpublikasi: **Adm**
10. Melihat artikel: **V, L, Aut, Adm**
11. Comment dan Likes: **L, Aut, Adm**
12. Melihat halaman utama vaksin: **V, L, Adm**
14. Melihat detail vaksin: **L, Adm**
