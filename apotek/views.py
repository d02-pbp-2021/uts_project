
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Obat
from .forms import ObatForm
from django.contrib.auth.decorators import login_required
from account.decorator import allowed_users
from django.core import serializers
import json as JSON

def apotek(request):
        obat = Obat.objects.all().order_by('nama_obat')
        response = {'obat':obat}
        return render(request, 'apotek.html', response)

@login_required(login_url='/login')
@allowed_users(allowed_roles=["Seller"])
def add_obat(request):
    forms = ObatForm()
    if request.method == 'POST':
        forms = ObatForm(request.POST)
        if forms.is_valid():
            forms.save()
            return redirect('apotek')
    context = {'forms':forms}
    return render(request, 'obat_form.html', context)

# Referensi: https://youtu.be/-oLVZp1NQVE
def auto_complete_sb(request):
    if 'term' in request.GET:
        obat = Obat.objects.all().order_by('nama_obat').filter(nama_obat__icontains=request.GET.get('term'))
        daftar_obat = list()
        for i in obat:
            daftar_obat.append(i.nama_obat)
        return JsonResponse(daftar_obat, safe=False)
    return render(request, 'apotek.html')

# Referensi: https://youtu.be/VL5ZNCjXEbw
def cari_obat(request):
    if request.method == "GET":
        obat = Obat.objects.all().order_by('nama_obat').filter(nama_obat__icontains=request.GET.get('srcFill'))
    response = {'obat':obat}
    return render(request, 'apotek.html', response)

@csrf_exempt
def json(request):
    products = Obat.objects.all()
    data = serializers.serialize('json', products)
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def post(request):
    if (request.method == 'POST'):
        body = request.body.decode('utf-8')
        body_map = JSON.loads(body)
        nama = body_map["nama_obat"]
        desc = body_map['khasiat']
        lp = body_map['link_produk']
        lg = body_map['link_gambar']
        Obat.objects.create(nama_obat = nama, khasiat = desc, link_produk = lp, link_gambar = lg)
    return HttpResponseRedirect('/apotek/')
        