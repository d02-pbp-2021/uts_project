from django.urls import path
from .views import apotek, add_obat, auto_complete_sb, cari_obat, json, post

urlpatterns = [
    path('', apotek, name='apotek'),
    path('add-obat/', add_obat, name='tambahkan-obat'),
    path('autocomplete-sb/', auto_complete_sb, name='auto-complete-sb'),
    path('cari-obat/', cari_obat, name='cari-obat'),
    path('json/', json, name='json'),
    path('post/', post, name='post')
]

