from django.db import models

class Obat(models.Model):
    nama_obat = models.CharField(max_length=50)
    khasiat = models.CharField(max_length=50)
    link_produk = models.CharField(max_length = 500, null=True, blank=True)
    link_gambar = models.CharField(max_length = 500, null=True, blank=True)

