from django.forms import fields
from .models import Obat
from django import forms

class ObatForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.label_suffix = "" 

    class Meta:
        model = Obat
        fields = ('nama_obat', 'khasiat', 'link_produk', 'link_gambar')

        #Referensi: https://youtu.be/6-XXvUENY_8
        widgets = {
            'nama_obat': forms.TextInput(attrs={'class':'form-control'}),
            'khasiat': forms.TextInput(attrs={'class':'form-control'}),
            'link_produk': forms.URLInput(attrs={'class':'form-control'}),
            'link_gambar': forms.URLInput(attrs={'class':'form-control'}),
        }

        #Referensi: https://youtu.be/6-XXvUENY_8
        labels = {
            'nama_obat': 'Nama Obat',
            'khasiat': 'Khasiat',
            'link_produk': 'Link Pembelian',
            'link_gambar': 'Link Gambar',
        }
