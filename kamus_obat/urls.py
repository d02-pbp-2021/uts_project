from django.urls import path
from .views import delete_kamus, kamus, add_kamus, detil_kamus, load_data, search_kamus, delete_kamus, json_kamus, post_kamus, delete_flutter

app_name = 'kamus_obat'

urlpatterns = [
    path('', kamus, name='kamus-obat'),
    path('add-kamus/', add_kamus, name='add-kamus'),
    path('obat/<int:pk>', detil_kamus, name='detil-kamus'),
    path('load-more/', load_data, name="load-more"),
    path('search/', search_kamus, name="search-kamus"),
    path('obat/<int:pk>/delete', delete_kamus, name='delete-kamus'),
    path('json/', json_kamus, name="kamus-json"),
    path('post/', post_kamus, name="kamus-post"),
    path('delete-post/', delete_flutter, name="delete-post"),

]
