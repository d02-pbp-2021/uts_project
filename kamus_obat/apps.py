from django.apps import AppConfig


class KamusObatConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kamus_obat'
