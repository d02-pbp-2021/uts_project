from django.db import models

# Create your models here.
class Kamus(models.Model):
    # image = models.ImageField(null=True, blank=True, upload_to='images/')
    image = models.URLField(max_length=600, blank=True, null=True)
    name = models.CharField(max_length=30, default='')
    description = models.TextField(default='')
    effect = models.CharField(max_length=100, default='')
    ingredients = models.TextField(default='')
