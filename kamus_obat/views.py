from django.shortcuts import render

# Create your views here.
from .models import Kamus
from .forms import KamusForm
from django.http.response import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from django.template.loader import render_to_string
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import json
from django.http import QueryDict



@login_required(login_url='/admin/login/')
def add_kamus(request):
    form = KamusForm(request.POST, request.FILES)

    response = {
        'form' : form,
    }

    if (form.is_valid() and request.method == "POST"):
        form.save()
    
        c_id = int(Kamus.objects.latest('id').id)
        return HttpResponseRedirect("/kamus-obat/obat/{id}".format(id=c_id))

    return render(request, 'kamus_obat/kamus_form.html', response)



def kamus(request):
    total_data=Kamus.objects.count()
    kamus = Kamus.objects.all().values()[:6]
    response = {
        "kamus" : kamus,
        "total_data" : total_data,
        }
    return render(request, "kamus_obat/kamus.html", response)

def load_data(request):
    current = int(request.GET['current'])
    limit = int(request.GET['limit'])
    kamus = Kamus.objects.all().values()[current:current+limit]

    response=render_to_string('ajax/kamus.html',{'kamus':kamus})
    return JsonResponse({'kamus':response})

## Referensi: https://www.youtube.com/watch?v=8NPOwmtupiI&list=PLCC34OHNcOtr025c1kHSPrnP18YPB-NFi&index=7
@login_required(login_url='/admin/login/')
def delete_kamus(request, pk):
    kamus = Kamus.objects.get(id=pk)

    if (request.method =='POST'):
        kamus.delete()
        return HttpResponseRedirect('/kamus-obat/')
    response = {"kamus" : kamus}
    return render(request, "kamus_obat/kamus_delete.html", response)

## Referensi: https://www.youtube.com/watch?v=ZHSXaFfx84I&t=453s
def search_kamus(request):
    query = request.GET['query']
    kamus = Kamus.objects.filter(name__icontains=query)
    total = kamus.count()
    response = {
        "kamus" : kamus,
        "total" : total,
        
        }
    return render(request, "kamus_obat/kamus_search.html", response)


def detil_kamus(request, pk):
    kamus = Kamus.objects.get(id=pk)
    response = {"kamus" : kamus}
    return render(request, "kamus_obat/kamus_detil.html", response)


@csrf_exempt
def json_kamus(request):
    products = Kamus.objects.all()
    data = serializers.serialize('json', products)
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def post_kamus(request):
    if(request.method=='POST'):
        request_data = request.body
        request_data = json.loads(request_data.decode('utf-8'))
        data = QueryDict('',mutable=True)
        data.update(request_data)
        form_req = KamusForm(data)

        if form_req.is_valid():
            form_req.save()
        
        return HttpResponseRedirect("/kamus-obat/")

@csrf_exempt
def delete_flutter(request):
    if (request.method =='POST'):
        request_data = request.body
        pk = json.loads(request_data.decode('utf-8'))['pk']
        kamus = Kamus.objects.get(id=int(pk))
        kamus.delete()
        
    return HttpResponseRedirect('/kamus-obat/')
    


