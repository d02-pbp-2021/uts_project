// Referensi: https://www.youtube.com/watch?v=QVRm4_njFIQ

$(document).ready(function() {
    var _current=$(".card-container").length;
    $("#loadMore").on('click', function() {

        var _current=$(".card-container").length;
        var _limit=$(this).attr('data-limit');
        var _total=$(this).attr('data-total');
        

        $.ajax({
			url:'load-more/',
			data:{
				limit:_limit,
				current:_current,
			},
			dataType:'json',
			beforeSend:function(){
				$("#loadMore").attr('disabled',true);
			},
			success:function(res){
                $("#products").append(res.kamus);
				$("#loadMore").attr('disabled',false);

				var _totalShowing=$(".card-container").length;
				if(_totalShowing==_total){
					$("#loadMore").remove();
				}
			}
		});

    });
});