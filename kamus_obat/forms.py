from django import forms
from .models import Kamus

class KamusForm(forms.ModelForm):
    class Meta:
      model = Kamus
      fields = [
        'image',
        'name',
        'description',
        'effect',
        'ingredients',
      ]
    
    widgets = {
      'image': forms.URLInput(attrs={'class': 'formEntry'}),
    }

    ## Membuat attribut untuk setiap field
    # widgets = {
    #   'gambar': forms.FileInput(
    #     attrs = {
    #       'type' : 'image',
    #       'class': 'form-control-file',
    #     }
    #   ),
      
    # }

   