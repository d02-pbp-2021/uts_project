# Generated by Django 3.2.8 on 2021-10-26 04:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kamus_obat', '0004_alter_kamus_image'),
    ]

    operations = [
        migrations.RenameField(
            model_name='kamus',
            old_name='howto',
            new_name='description',
        ),
    ]
