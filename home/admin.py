from django.contrib import admin
from .models import DiscussionReply
from .models import DiscussionThread
from .models import NestedReply

admin.site.register(DiscussionThread)
admin.site.register(DiscussionReply)
admin.site.register(NestedReply)