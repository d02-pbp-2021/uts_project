from django import forms
from .models import DiscussionThread

class ThreadForm(forms.ModelForm):
    class Meta:
        model = DiscussionThread
        fields = ["topic", "content", "check"]

    def __init__(self, *args, **kwargs):
        super(ThreadForm, self).__init__(*args, **kwargs)
    