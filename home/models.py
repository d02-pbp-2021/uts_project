from django.db import models
from django.utils.text import slugify
from datetime import datetime
import uuid
"""
from django.db.models.deletion import CASCADE
from django.contrib.auth.models import User
"""

class NestedReply(models.Model):
    name = models.CharField(max_length = 100, default = "Anonymous", blank = True)
    date_created =  models.DateTimeField(auto_now_add = True , null = True)
    content = models.CharField(max_length = 1000)
    check = models.BooleanField(blank = True, default = False)
    uid = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False, blank = True)

    def __str__(self):
        return "Nested Reply: " + str(self.uid)
        
    class Meta:
        verbose_name_plural = "Nested Replies"
        ordering = ('date_created', )

class DiscussionReply(models.Model):
    name = models.CharField(max_length = 100, default = "Anonymous", blank = True)
    date_created =  models.DateTimeField(auto_now_add = True, null = True)
    content = models.CharField(max_length = 1000)
    replies = models.ManyToManyField(NestedReply, blank = True)
    check = models.BooleanField(blank = True, default = False)
    uid = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False, blank = True, unique = True)

    @property
    def ide(self):
        return self.uid.hex

    def __str__(self):
        return "Discussion Reply: " + str(self.uid)

    class Meta:
        verbose_name_plural = "Discussion Replies"
        ordering = ('date_created', )

class DiscussionThread(models.Model):
    topic = models.CharField(max_length = 100)
    slug = models.SlugField(max_length = 400, unique = True, blank = True)
    name = models.CharField(max_length = 100, default = "Anonymous", blank = True)
    date_created =  models.DateTimeField(auto_now_add = True, null = True)
    content = models.CharField(max_length = 1000)
    replies = models.ManyToManyField(DiscussionReply, blank = True)
    check = models.BooleanField(blank = True, default = False)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.topic) + "-" + str(DiscussionThread.objects.count())
        super(DiscussionThread, self).save(*args, **kwargs)

    def __str__(self):
        return self.slug

    class Meta:
        verbose_name_plural = "Discussion Threads"
        ordering = ('date_created', )

"""
class Thread(models.Model):
    topic = models.CharField(max_length=500)
    # author = models.ForeignKey(User, on_delete=CASCADE)
    author = models.CharField(max_length = 500, default = "Anonymous", blank = True)
    date_created =  models.DateTimeField(default = datetime.now(), null = True)
    content = models.CharField(max_length = 5000)
    uid = models.UUIDField(primary_key = True, default = uuid.uuid4, editable = False, blank = True, unique = True)
    connection = models.ForeignKey(Connect, on_delete=CASCADE)

    # @property
    # def get_author(self):
    #     return self.author.username

class Connect(models.Model):
    child = models.OneToManyField(Thread, blank=True)
"""