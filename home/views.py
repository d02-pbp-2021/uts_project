from django.shortcuts import redirect, render, get_object_or_404
from django.core import serializers
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from .models import DiscussionThread
from .models import DiscussionReply
from .models import NestedReply
import uuid
import json

def home(request):
    threads = DiscussionThread.objects.all().values()

    response = {"Threads": threads}
    return render(request, "home.html", response)

def forum(request, slug):
    thread = get_object_or_404(DiscussionThread, slug = slug)
    user = request.user

    if "reply-thread" in request.POST:
        if user.is_authenticated:
            content = request.POST.get("content")
            check = request.POST.get("check")

            if check == "on":
                temp = DiscussionReply.objects.create(content=content, check=True)
            else:
                temp = DiscussionReply.objects.create(content=content, name=user.username)

            thread.replies.add(temp)
        
        else:
            return redirect('/login')
    
    elif "comment-thread" in request.POST:
        if user.is_authenticated:
            content = request.POST.get("content")
            check = request.POST.get("check")

            obj = DiscussionReply.objects.get(uid=uuid.UUID(request.POST.get("getID")))

            if check == "on":
                temp = NestedReply.objects.create(content=content, check=True)
            else:
                temp = NestedReply.objects.create(content=content, name=user.username)

            
            obj.replies.add(temp)
        
        else:
            return redirect('/login')

    data = {
        "main": thread
    }

    return render(request, "forum.html", data)

def load(request):
    index = int(request.POST["index"])
    end = 5 # Load 5 Thread

    return JsonResponse(data = {
        "show": serializers.serialize("json", DiscussionThread.objects.all()[index: index + end]),
        "limit": DiscussionThread.objects.count()
        })

@login_required(login_url="/login")
def add_thread(request):
    user = request.user

    if "add_thread_input" in request.POST:
        if user.is_authenticated:
            topic = request.POST.get("topic")
            content = request.POST.get("content")
            check = request.POST.get("check")
            
            if check == "on":
                DiscussionThread.objects.create(topic=topic, content=content, check=True)
            else:
                DiscussionThread.objects.create(topic=topic, content=content, name=user.username)
            
            return redirect('/')
        
        else:
            return redirect('/login')

    return render(request, "submit_thread.html")

# @csrf_exempt
# def test(request):
#     return JsonResponse(data = {
#         "Juan": "Satu",
#         "TEST": "test"
#     })

@csrf_exempt
def test(request):
    data = json.loads(request.body)
    topic = data["topic"]
    content = data["content"]
    name = data["name"]

    DiscussionThread.objects.create(topic=topic, content=content, name=name)
    return JsonResponse(
        data = {
            "topic": topic,
            "content": content,
            "name": name
        }
    )

@csrf_exempt
def load_flutter(request):
    data = json.loads(request.body)

    index = int(data["index"])
    end = 5 # Load 5 Thread

    return JsonResponse(
        data = {
            "show": serializers.serialize("json", DiscussionThread.objects.all()[index: index + end]),
        }
    )