from django.urls import path
from .views import home
from .views import load
from .views import forum
from .views import add_thread
from .views import test
from .views import load_flutter

urlpatterns = [
    path("load/", load, name = "load"),
    path("forum/<slug>/", forum, name = "forum"),
    path("Add-Thread/", add_thread, name = "add_thread"),
    path("test/", test, name = "test"),
    path("load-flutter/", load_flutter, name = "load_flutter"),
]